const imgs = Array.from( document.querySelectorAll('img') );
imgs.forEach(image => {
    let alt = image.getAttribute('alt');
    if( alt ) {
        let fig = document.createElement('figure');
        fig.setAttribute('class', 'full');
        let caption = document.createElement('figcaption');
        caption.insertAdjacentHTML('afterbegin', `${alt}`);
        fig.appendChild(image.cloneNode());
        fig.appendChild(caption);
        image.replaceWith(fig);
    }
});
