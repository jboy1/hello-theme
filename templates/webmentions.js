// adapted from evert/evert.github.com

if (document.readyState === 'interactive' || document.readyState === 'complete') {
  loadWebMentions();
} else {
  document.addEventListener("DOMContentLoaded", function(event) {
    loadWebMentions();
  });
}

function loadWebMentions() {

  if (typeof fetch === "undefined") {
    // We don't support browsers that don't have fetch
    return;
  }

  var elem = document.getElementsByClassName('webmentions')[0];
  if (!elem) {
    // Not supported on this page
    return;
  }

  var url = document.URL;

  return fetch('https://webmention.io/api/mentions?per-page=200&target=' + url)
    .then( response => response.json() )
    .then( result => displayWebMentions(elem, result) );

}

function displayWebMentions(elem, result) {

  if (!result.links.length) {
    console.log('Found no webmentions for this page.');
    return;
  }

  var verbHtml = [];

  result.links.sort( (a, b) => {
    return a.data.published_ts - b.data.published_ts
  });

  for(var linkIdx in result.links) {
    var link = result.links[linkIdx];

    if (!link.data.author) {
      console.log('Author-less webmentions are not supported.');
      continue;
    }

    switch(link.activity.type) {
      case 'like' :
        verbHtml.push(link.data.author.name + ' ' + linkVerb('liked', link) + ' this');
        break;
      case 'link' :
      case 'repost' :
        verbHtml.push(link.data.author.name + ' ' + linkVerb('shared', link) + ' this');
        break;
      case 'reply' :
        verbHtml.push(link.data.author.name + ' ' + linkVerb('commented on', link) + ' this');
        break;
      default :
        console.log('Ignoring this activity type.');
        break;

    }
  }

  if (result.links.length == 1) {
    var html = '<p>Found one reaction.</p>';
  }
  else {
    var html = '<p>Found ' + result.links.length + ' reactions.</p>';
  }

  if (verbHtml.length) {
    html += '<p>' + verbHtml.join(' {{ LOGO_CHAR }} ') + '.</p>\n';
  }

  elem.innerHTML = html;
  document.getElementById('webmentions').style.display = 'block';

}

function linkVerb(verb, link) {

  if (link.data && link.data.author) {
    return '<a href="' + h(link.data.url) +
           '" title="' + h(link.data.author.name) +
           ' @ ' + h(link.data.published) +
           '" rel="nofollow">' + verb + '</a>';
  }

}

function h(input) {

  if (!input) {
    return '';
  }
  var charsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;'
  };

  return input.replace(/&|<|>|"/g, char => {

    return charsToReplace[char];

  });

}
