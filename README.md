# hello pelican!

This is a theme for the [Pelican][] static site generator based on arp242's
[Hello CSS][] project.

## Features

The theme is highly readable and responsive.

It currently has support for blog posts and static pages, but doesn't handle
tag, category or archive pages. That's simply because the author doesn't have
need for them at present.

In browsers that support [font
variations](https://caniuse.com/#feat=font-feature), the theme will use the
included [Inter font][], a beautiful sans-serif font.

The theme has support for [webmentions][].

## Licenses

**Hello CSS** has been released in the public domain and can be used without
any restrictions. **Inter** is distributed under the [SIL Open Font License
1.1][].

The included feed icon and site icons are free for non-commercial use under the
terms of [this license](https://iconmonstr.com/license/). The infinity sign is
[from Inter](https://rsms.me/inter/glyphs/?g=infinity).

The theme itself is [MIT-licensed][MIT License].

**∞**

[Pelican]: http://getpelican.com/
[Hello CSS]: https://github.com/arp242/hello-css/ "Hello, CSS!"
[Inter font]: https://rsms.me/inter/
[SIL Open Font License 1.1]: https://choosealicense.com/licenses/ofl-1.1/
[MIT License]: https://choosealicense.com/licenses/mit/
[webmentions]: https://webmention.io/
